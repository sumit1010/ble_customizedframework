//
//  ViewController+ TableViewConfiguration.swift
//  BLEDemoTest
//
//  Created by Sumit Vishwakarma on 25/05/23.
//

import Foundation
import UIKit
import ChipMateBLEFrameWork
import CoreBluetooth


extension ViewController : UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayOfPeripherals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! UserCell
        
        cell.lableBLEDeviceName.text = self.arrayOfPeripherals[indexPath.row].name ?? "unknown device"
        cell.lableIdentifier.text = "\(self.arrayOfPeripherals[indexPath.row].identifier)"
        cell.btn_DisconnectBLE.tag = indexPath.row
        cell.btn_DisconnectBLE.addTarget(self,action:#selector(button_DisconnectAction), for:.touchUpInside)
        cell.selectionStyle = .none
        cell.lableState.textColor = .red
        
        switch self.arrayOfPeripherals[indexPath.row].state {
        case .connected:
            cell.lableState.text = "Connected 👍🏻"
            cell.lableState.textColor = .blue
            cell.btn_DisconnectBLE.isHidden = false
            cell.btn_DisconnectBLE.layer.cornerRadius = 12
            cell.activityLoader.isHidden = true
            
            UIView.animate(withDuration: 0.6, animations: {
                self.ShowHideConnectDeivecText(isHide: false)
                cell.btn_DisconnectBLE.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                cell.lableState.transform = CGAffineTransform(scaleX: 0.9, y: 0.9) }, completion: { _ in
                    UIView.animate(withDuration: 0.8) {
                        cell.btn_DisconnectBLE.transform = CGAffineTransform.identity
                        cell.lableState.transform = CGAffineTransform.identity
                    }
                })
        case .connecting:
            cell.lableState.text = "Connecting..."
            cell.btn_DisconnectBLE.isHidden = true
            cell.activityLoader.isHidden = false
            cell.activityLoader.startAnimating()
            
        case .disconnected:
            cell.lableState.text = "Disconnected"
            cell.btn_DisconnectBLE.isHidden = true
            cell.activityLoader.isHidden = true
            
        case .disconnecting:
            cell.lableState.text = "Disconnecting"
            cell.btn_DisconnectBLE.isHidden = true
            cell.activityLoader.isHidden = false
            cell.activityLoader.startAnimating()
            
        default:
            print("Not connected")
        }
        return cell
    }
    
    @objc func button_DisconnectAction(sender:UIButton) {
        print(sender.tag)
        if self.arrayOfPeripherals[sender.tag].state == .connected {
            bleSupporter?.diconnectPeripheral(peripheral: self.arrayOfPeripherals[sender.tag])
        }
        
    }
}


extension ViewController :  UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.peripheral = self.arrayOfPeripherals[indexPath.row]
        for device in self.arrayOfPeripherals {
            if device.state == .connected {
                bleSupporter?.diconnectPeripheral(peripheral: device)
            }
        }
        bleSupporter?.connect(peripheral: self.peripheral)
        print(self.peripheral.name ?? "")
        
        self.reloadTableView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160.0
    }
}

 
