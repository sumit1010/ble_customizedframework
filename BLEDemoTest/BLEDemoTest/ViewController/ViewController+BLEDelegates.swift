//
//  ViewController+BLEDelegates.swift
//  BLEDemoTest
//
//  Created by Sumit Vishwakarma on 25/05/23.
//

import Foundation
import UIKit
import ChipMateBLEFrameWork
import CoreBluetooth


extension ViewController: blueToothSupporterDelegate {
    func recentDiconnectedPeripheral(device: CBPeripheral) {
        self.ShowHideConnectDeivecText(isHide: true)
        self.reloadTableView()
    }
        
    func detectedPeripheralsList(BLEDevicesList: [CBPeripheral]) {
         self.arrayOfPeripherals = BLEDevicesList/*.filter({($0.name != nil)})*/
        self.HeaderTitleWithCound.text = "BLE Devices List (" + "\(BLEDevicesList.count)" + ")"
        self.reloadTableView()
        
    }
    
    func isBluetoothOn(isOn: Bool) {
        self.isBluetoothOn = isOn
        print(isOn ? "Bluetooth is On" : "Please turn on your bluetooth.")
    }
    
    func connectedDeviceIs(device: CBPeripheral) {
        self.lableConnectedDeviceName.text = device.name
        for i in 0..<self.arrayOfPeripherals.count {
            if self.arrayOfPeripherals[i] == device {
                self.arrayOfPeripherals[i] = device
            }
        }
        self.reloadTableView()
    }
    
    func reloadTableView(){
        DispatchQueue.main.async {
            self.tableViewBLEDevices.reloadData()
        }
    }
}
