//
//  ViewController.swift
//  BLEDemoTest
//
//  Created by Sumit Vishwakarma on 25/05/23.
//

import UIKit
import ChipMateBLEFrameWork
import CoreBluetooth

class ViewController: UIViewController   {
    
    @IBOutlet weak var HeaderTitleWithCound: UILabel!
    @IBOutlet weak var tableViewBLEDevices: UITableView!
    var bleSupporter : blueToothSupporter?
    var arrayOfPeripherals: [CBPeripheral] = []
    var isBluetoothOn: Bool = false
    var peripheral: CBPeripheral!
    
    @IBOutlet weak var lblConnected: UILabel!
    @IBOutlet weak var lableConnectedDeviceName: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bleSupporter = blueToothSupporter.init()
        bleSupporter?.delegate = self
        self.ShowHideConnectDeivecText(isHide: true)
    }
    
    func ShowHideConnectDeivecText(isHide: Bool) {
        self.lblConnected.isHidden = isHide
        self.lableConnectedDeviceName.isHidden = isHide
    }
}



